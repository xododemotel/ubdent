/**
 * Ubdent App by Ubdent
 * Created by: Leandro Maciel
 * Version: 0.1.0
 */

import { AppRegistry, Text } from 'react-native'
import App from './src/App'
import { name as appName } from './app.json'

Text.defaultProps = Text.defaultProps || {}
Text.defaultProps.allowFontScaling = false

AppRegistry.registerComponent(appName, () => App)
