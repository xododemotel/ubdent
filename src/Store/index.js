import React from 'react'
import useGlobalHook from './UseGlobalHook'
import * as actions from '../Actions'
import state from '../States'

export const useGlobal = useGlobalHook(React, state, actions)

export default useGlobal
