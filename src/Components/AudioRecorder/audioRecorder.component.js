import React, { Component } from 'react';
import { StyleSheet, View, Alert } from 'react-native';
import { Buffer } from 'buffer';
import Permissions from 'react-native-permissions';
import Sound from 'react-native-sound';
import AudioRecord from 'react-native-audio-record';
import initialState from '../../States'
import { Button } from 'react-native-elements'
import axios from 'axios'
import base64 from 'base64-js'
import RNFS from 'react-native-fs'
import ComponentStyle from '../../Styles/component.style'

export default class AudioRecorder extends Component {
  sound = null
  state = {
    audioFile: '',
    recording: false,
    loaded: false,
    paused: true,
    base64File: []
  }

  async componentDidMount() {
    await this.checkPermission();

    const options = {
      sampleRate: 8000,
      channels: 1,
      bitsPerSample: 8,
      wavFile: `${this.props.fileName}.wav`
    };

    AudioRecord.init(options)
  }

  checkPermission = async () => {
    const permission = await Permissions.check('microphone');
    console.log('permission check', permission);
    if (permission === 'authorized') return;
    return this.requestPermission();
  };

  requestPermission = async () => {
    const permission = await Permissions.request('microphone');
    console.log('permission request', permission);
  };

  start = () => {
    console.log('start record');
    this.setState({ audioFile: '', recording: true, loaded: false });
    AudioRecord.start();
  };

  stop = async () => {
    if (!this.state.recording) return;
    console.log('stop record');
    let audioFile = await AudioRecord.stop();
    console.log('audioFile', audioFile);
    this.setState({ audioFile, recording: false });
  };

  load = () => {
    return new Promise((resolve, reject) => {
      if (!this.state.audioFile) {
        return reject('file path is empty');
      }

      this.sound = new Sound(this.state.audioFile, '', error => {
        if (error) {
          console.log('failed to load the file', error);
          return reject(error);
        }
        console.log(this.state.audioFile)
        this.setState({ loaded: true });
        return resolve();
      });
    });
  };

  play = async () => {
    if (!this.state.loaded) {
      try {
        await this.load();
      } catch (error) {
        console.log(error);
      }
    }

    this.setState({ paused: false });
    Sound.setCategory('Playback');

    this.sound.play(success => {
      if (success) {
        console.log('successfully finished playing');
      } else {
        console.log('playback failed due to audio decoding errors');
      }
      this.setState({ paused: true });
      // this.sound.release();
    });
  };

  pause = () => {
    this.sound.pause();
    this.setState({ paused: true });
  };

  uploadAudio = async () => {
    try {
      const fileContents = await RNFS.readFile(this.state.audioFile, 'base64')
      const arrayBase64 = Buffer.from(fileContents, 'base64')
      const base64File = base64.fromByteArray(arrayBase64)
      console.log('fazendo upload...')
      const { config, endpoint } = initialState
      const axiosConfig = {
        method: endpoint.uploadFile.method,
        baseURL: config.api.baseURL,
        url: endpoint.uploadFile.endpoint.replace('{{CPF}}', this.props.fileName),
        headers: {
          API_KEY: config.api.key
        },
        data: {
          binary: base64File,
          fileName: `${this.props.fileName}-audio`,
          fileFormat: 'wav'
        }
      }

      return axios.request(axiosConfig)
        .then(response => [null, response])
        .catch(error => [error])
    } catch (error) {
      console.log(error)
    }
  }

  sendAudio = async () => {
    try {
      const [error, response] = await this.uploadAudio()

      if (response) {
        this.props.handlers.onPress()
      } else {
        console.log(error)
        Alert.alert(
          'OPS',
          'Houve um erro ao enviar o seu áudio. Por favor tente novamente.',
          [
            { text: 'OK' }
          ],
          { cancelable: false }
        )
      }
    } catch (error) {
      console.log(error)
    }
  }

  render() {
    const { recording, paused, audioFile } = this.state;
    console.log()
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.container}>
          {this.props.controlsVisible ? (
            <View style={styles.row}>
              <Button buttonStyle={ComponentStyle.primaryButton} onPress={this.start} title="Gravar" disabled={recording} />
              <Button buttonStyle={ComponentStyle.primaryButton} onPress={this.stop} title="Parar" disabled={!recording} />
              {paused ? (
                <Button buttonStyle={ComponentStyle.primaryButton} onPress={this.play} title="Tocar" disabled={!audioFile} />
              ) : (
                <Button buttonStyle={ComponentStyle.primaryButton} onPress={this.pause} title="Pausar" disabled={!audioFile} />
              )}
            </View>
          ) :
            null
          }
        </View>
        <View style={{ flex: 1 }}>
          {
            (!this.props.buttonDisabled) &&
              <View style={ComponentStyle.primaryButtonView}>
                <Button
                  raised
                  title='ENVIAR'
                  disabled={false}
                  onPress={this.sendAudio}
                  buttonStyle={ComponentStyle.primaryButton}
                />
              </View>
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 6,
    justifyContent: 'center'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  }
});