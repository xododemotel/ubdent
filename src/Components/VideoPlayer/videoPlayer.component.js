import React, { Component } from 'react'
import { StyleSheet, View, TouchableWithoutFeedback } from 'react-native';
import Video from 'react-native-video'
import video from '../../States/video.state'

export default class VideoPlayerComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ...video
    }

    this.video = Video
  }

  validateVideo = () => {
    try {

      let video  = null
      let videoName = (this.props.videoExample) ? this.props.handlers.videoExample : this.props.handlers.video

      switch(videoName) {
        case '1-nome-completo':
          video = require('../../Assets/videos/1-nome-completo.mp4')
        break
        case '2-dados-usuario':
          video = require('../../Assets/videos/2-dados-usuario.mp4')
        break
        case '3-sintomas':
          video = require('../../Assets/videos/3-sintomas.mp4')
        break
        case '4-foto-sorriso-frontal':
          video = require('../../Assets/videos/4-foto-sorriso-frontal.mp4')
        break
        case '5-foto-sorriso-video-exemplo':
          video = require('../../Assets/videos/5-foto-sorriso-video-exemplo.mp4')
        break
        case '6-foto-ceu-da-boca':
          video = require('../../Assets/videos/6-foto-ceu-da-boca.mp4')
        break
        case '7-foto-dentes-de-baixo':
          video = require('../../Assets/videos/7-foto-dentes-de-baixo.mp4')
        break
        case '8-ultima-visita-dentista':
          video = require('../../Assets/videos/8-ultima-visita-dentista.mp4')
        break
        case '9-medo-de-dentista':
          video = require('../../Assets/videos/9-medo-de-dentista.mp4')
        break
        case '10-ultima-ida-ao-medico':
          video = require('../../Assets/videos/10-ultima-ida-ao-medico.mp4')
        break
        case '11-problema-de-saude':
          video = require('../../Assets/videos/11-problema-de-saude.mp4')
        break
        case '12-toma-algum-medicamento':
          video = require('../../Assets/videos/12-toma-algum-medicamento.mp4')
        break
        case '13-possui-alergia':
          video = require('../../Assets/videos/13-possui-alergia.mp4')
        break
        case '14-pressao-alta':
          video = require('../../Assets/videos/14-pressao-alta.mp4')
        break
        case '15-diabetes':
          video = require('../../Assets/videos/15-diabetes.mp4')
        break
        case '16-problema-do-coracao':
          video = require('../../Assets/videos/16-problema-do-coracao.mp4')
        break
        case '17-gravida':
          video = require('../../Assets/videos/17-gravida.mp4')
        break
        case '18-agradecimento':
          video = require('../../Assets/videos/18-agradecimento.mp4')
        break
        case 'demonstracao':
          video = require('../../Assets/videos/demonstracao.mp4')
        break
      }
      return video
    } catch (error) {
      console.log(error)
    }
  }

  // video ends
  onEnd = () => {
    this.setState({ paused: true })
    this.props.handlers.onEndVideo()
  }

  // on press video event
  onPressVideo = () => {
    try {
      this.setState({
        paused: !this.state.paused
      })
    } catch (error) {
      console.log(error)
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback
          style={styles.fullScreen}
          // onPress={() => this.onPressVideo()}>
        >
          <Video
            ref={(ref: Video) => { this.video = ref }}
            source={this.validateVideo()}
            style={styles.fullScreen}
            rate={this.state.rate}
            paused={this.state.paused}
            volume={this.state.volume}
            muted={this.state.muted}
            resizeMode={this.state.resizeMode}
            onLoad={this.onLoad}
            onProgress={this.onProgress}
            onEnd={this.onEnd}
            onAudioBecomingNoisy={this.onAudioBecomingNoisy}
            onAudioFocusChanged={this.onAudioFocusChanged}
            repeat={false}
          />
        </TouchableWithoutFeedback>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    height: 240
  },
  fullScreen: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: 240
  }
})
