export const cleanDocument = (value) => {
  let document = value.replace(/\./g, '')
  document = document.replace(/-/g, '')
  document = document.replace(/\//g, '')
  return document
}
