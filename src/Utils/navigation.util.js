import { StackActions, NavigationActions } from 'react-navigation'

export const resetStackNavigation = (index = 0, stack = [], navigation) => {
  try {
    const actions = []
    stack.map((item) => {
      actions.push(NavigationActions.navigate({ routeName: item }))
    })
    const resetAction = StackActions.reset({
      index,
      actions
    })
    navigation.dispatch(resetAction)
  } catch (error) {
    console.log(error)
  }
}
