import { Stitch, AnonymousCredential, RemoteMongoClient } from 'mongodb-stitch-react-native-sdk'

export const connect = async (store) => {
  try {
    Stitch.initializeDefaultAppClient(store.state.config.mongodbStich.appId).then(client => {
      store.actions.updateConfigState('mongodbStich', 'client', client)
      store.state.config.mongodbStich.client.auth
        .loginWithCredential(new AnonymousCredential())
        .then(user => {
          store.actions.updateConfigState('mongodbStich', 'userId', user.id)
          return true
        })
        .catch(err => {
          console.log(`Failed to log in anonymously: ${err}`)
          store.actions.updateConfigState('mongodbStich', 'userId', null)
          return false
        })
    })
  } catch (error) {
    console.log(error)
  }
}

export const getCollection = (store, collection) => {
  try {
    const stitchAppClient = Stitch.defaultAppClient
    const mongoClient = stitchAppClient.getServiceClient(RemoteMongoClient.factory, store.state.config.mongodbStich.appClient)
    const document = mongoClient.db(store.state.config.mongodbStich.database).collection(collection)

    return document
  } catch (error) {
    console.warn(error)
  }
}

export const insertNewData = async (store, collection, data) => {
  try {
    const document = getCollection(store, collection)

    const newData = await document
      .insertOne({ data, owner_id: store.state.config.mongodbStich.userId })
      .then((result) => {
        return result
      })
      .catch(error => {
        console.warn(error)
        return false
      })

    if (newData) {
      return newData
    }
  } catch (error) {
    console.warn(error)
  }
}

export const updateDataById = async (store, collection, id, data) => {
  try {
    const document = getCollection(store, collection)
    const newData = await document
      .updateOne(
        { _id: id },
        { $set: { data: data } },
        { upsert: false }
      )
      .then((result) => {
        return result
      })
      .catch(error => {
        console.warn(error)
      })
    if (newData) {
      return newData
    }
  } catch (error) {
    console.warn(error)
  }
}
