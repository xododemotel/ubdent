import axios from 'axios'
import { cleanDocument } from '../Utils/string.util'

export const updateUserState = (store, property, value) => {
  try {
    store.setState({
      ...store.state,
      user: {
        ...store.state.user,
        [property]: value
      }
    })
  } catch (error) {
    console.log(error)
  }
}

export const clearUserState = (store) => {
  try {
    updateUserState(store, 'name', null)
    updateUserState(store, 'document', null)
    updateUserState(store, 'birthDay', '')
    updateUserState(store, 'birthMonth', '')
    updateUserState(store, 'birthYear', '')
    updateUserState(store, 'email', null)
    updateUserState(store, 'gender', null)
    updateUserState(store, 'phone', '+55')
  } catch (error) {
    console.log(error)
  }
}

export const createNewUser = async (store) => {
  try {
    const { user, config, endpoint } = store.state

    const axiosConfig = {
      method: endpoint.createUser.method,
      baseURL: config.api.baseURL,
      url: endpoint.createUser.endpoint,
      headers: {
        API_KEY: config.api.key
      },
      data: {
        name: user.name,
        cpf: user.document
      }
    }

    return axios.request(axiosConfig)
      .then(response => [null, response])
      .catch(error => [error])
  } catch (error) {
    console.log(error)
  }
}

export const addMedicalData = async (store) => {
  try {
    const { user, config, endpoint, anamnese } = store.state
    const axiosConfig = {
      method: endpoint.addMedicalData.method,
      baseURL: config.api.baseURL,
      url: endpoint.addMedicalData.endpoint.replace('{{CPF}}', user.document),
      headers: {
        API_KEY: config.api.key
      },
      data: {
        name: user.name,
        document: user.document,
        birthDay: user.birthDay,
        birthMonth: user.birthMonth,
        birthYear: user.birthYear,
        email: user.email,
        gender: user.gender,
        phone: user.phone,
        lastVisitDay: anamnese.lastVisitDay,
        lastVisitMonth: anamnese.lastVisitMonth,
        lastVisitYear: anamnese.lastVisitYear,
        dentistFear: anamnese.dentistFear,
        doctorLastVisitDay: anamnese.doctorLastVisitDay,
        doctorLastVisitMonth: anamnese.doctorLastVisitMonth,
        doctorLastVisitYear: anamnese.doctorLastVisitYear,
        healthProblems: anamnese.healthProblems,
        healthProblemsDetails: anamnese.healthProblemsDetails,
        takeMedication: anamnese.takeMedication,
        takeMedicationDetails: anamnese.takeMedicationDetails,
        haveAllergy: anamnese.haveAllergy,
        haveAllergyDetails: anamnese.haveAllergyDetails,
        lowPressure: anamnese.lowPressure,
        normalPressure: anamnese.normalPressure,
        highPressure: anamnese.highPressure,
        diabetes: anamnese.diabetes,
        hearthProblem: anamnese.hearthProblem,
        pregnant: anamnese.pregnant
      }
    }

    console.log(axiosConfig)

    return axios.request(axiosConfig)
      .then(response => [null, response])
      .catch(error => [error])
  } catch (error) {
    console.log(error)
  }
}
