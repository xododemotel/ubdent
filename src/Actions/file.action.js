import axios from 'axios'
import RNFS from 'react-native-fs'

export const uploadFile = async (store, filePath, suffix) => {
  try {
    const base64File = await RNFS.readFile(filePath, 'base64')
    console.log('fazendo upload...')
    const { config, endpoint, user } = store.state
    const axiosConfig = {
      method: endpoint.uploadFile.method,
      baseURL: config.api.baseURL,
      url: endpoint.uploadFile.endpoint.replace('{{CPF}}', user.document),
      headers: {
        API_KEY: config.api.key
      },
      data: {
        binary: base64File,
        fileName: `${user.document}-${suffix}`,
        fileFormat: 'jpg'
      }
    }

    return axios.request(axiosConfig)
      .then(response => [null, response])
      .catch(error => [error])
  } catch (error) {
    console.log(error)
  }
}
