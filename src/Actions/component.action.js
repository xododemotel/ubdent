export const updateComponentState = (store, nestedKey, property, value) => {
  try {
    const nestedWithPropertyChanged = {
      ...store.state.component[nestedKey],
      [property]: value
    }

    const component = {
      ...store.state.component,
      [nestedKey]: nestedWithPropertyChanged
    }

    store.setState({ ...store.state, component })
  } catch (error) {
    console.log(error)
  }
}
