export const updateAnamneseState = (store, property, value) => {
  try {
    store.setState({
      ...store.state,
      anamnese: {
        ...store.state.anamnese,
        [property]: value
      }
    })
  } catch (error) {
    console.log(error)
  }
}
