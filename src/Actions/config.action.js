export const updateConfigState = (store, nestedKey, property, value) => {
  try {
    const nestedWithPropertyChanged = {
      ...store.state.config[nestedKey],
      [property]: value
    }

    const config = {
      ...store.state.config,
      [nestedKey]: nestedWithPropertyChanged
    }

    store.setState({ ...store.state, config })
  } catch (error) {
    console.log(error)
  }
}
