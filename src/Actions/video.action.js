export const updateVideoState = (store, property, value) => {
  try {
    store.setState({
      ...store.state,
      video: {
        ...store.state.video,
        [property]: value
      }
    })
  } catch (error) {
    console.log(error)
  }
}
