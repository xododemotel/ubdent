export const updateInitialState = (store, property, value) => {
  try {
    store.setState({
      ...store.initialState,
      [property]: value
    })
  } catch (error) {
    console.log(error)
  }
}
