import React from 'react'
import { View, Image, Dimensions } from 'react-native'
import { Button } from 'react-native-elements'
import ComponentStyle from '../../Styles/component.style'

export function ForkScreen ({ handlers }) {
  return (
    <View style={{ flex: 1 }}>
      <View style={{ flex: 7 }}>
        <Image
          style={{ width: (Dimensions.get('window').width * 0.9), marginLeft: (Dimensions.get('window').width * 0.05) }}
          resizeMode='contain'
          source={require('../../Assets/logo.png')}
        />
      </View>
      <View style={ComponentStyle.primaryButtonView}>
        <Button
          raised
          title='COMEÇAR'
          onPress={() => handlers.onPress('Main')}
          buttonStyle={ComponentStyle.primaryButton}
        />
      </View>
    </View>
  )
}

export default ForkScreen
