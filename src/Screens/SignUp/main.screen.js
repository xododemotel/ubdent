import React from 'react'
import { View } from 'react-native'
import VideoPlayerComponent from '../../Components/VideoPlayer/videoPlayer.component'
import useGlobal from '../../Store'
import { Input, Button } from 'react-native-elements'
import ComponentStyle from '../../Styles/component.style'

export function MainScreen ({ handlers }) {
  const [state] = useGlobal()

  return (
    <View style={{ flex: 1 }}>
      <View style={{ flex: 2 }}>
        <VideoPlayerComponent handlers={handlers} />
      </View>
      <View style={{ flex: 6 }}>
        {
          (state.component.inputPrimary.visible) &&
            <View style={{ flex: 1 }}>
              <View style={{ flex: 7 }}>
                <Input
                  autoFocus={state.component.inputPrimary.autoFocus}
                  onChangeText={(name) => handlers.onChangeText('name', name)}
                  value={state.user.name}
                />
              </View>
              <View style={ComponentStyle.primaryButtonView}>
                <Button
                  raised
                  title='CONTINUAR'
                  disabled={state.component.buttonPrimary.disabled}
                  onPress={() => handlers.onPress()}
                  buttonStyle={ComponentStyle.primaryButton}
                />
              </View>
            </View>
        }
      </View>
    </View>
  )
}

export default MainScreen
