import React from 'react'
import { View, ScrollView, Text, Dimensions } from 'react-native'
import useGlobal from '../../Store'
import { Input, Button } from 'react-native-elements'
import PositionStyle from '../../Styles/position.style'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import VideoPlayerComponent from '../../Components/VideoPlayer/videoPlayer.component'
import ComponentStyle from '../../Styles/component.style'

export function LastVisitScreen ({ handlers }) {
  const [state] = useGlobal()

  return (
    <KeyboardAwareScrollView>
      <ScrollView contentContainerStyle={{ flexGrow: 1, height: Dimensions.get('window').height }} keyboardShouldPersistTaps='handled'>

        <View style={{ flex: 1 }}>
          <VideoPlayerComponent handlers={handlers} />
        </View>

        {
          (state.component.inputPrimary.visible) &&
            <View style={{ flex: 3 }}>
              <View style={{ flex: 2, marginLeft: (Dimensions.get('window').width * 0.05), marginRight: (Dimensions.get('window').width * 0.05) }}>

                <Text>Data da última visita ao dentista</Text>

                <View style={[PositionStyle.marginBottomBig, { flexDirection: 'row' }]}>
                  <View style={{ width: (Dimensions.get('window').width / 4) }}>
                    <Input
                      ref={(input) => { this.lastVisitDayRef = input }}
                      onSubmitEditing={() => { this.lastVisitMonthRef.focus() }}
                      blurOnSubmit={false}
                      placeholder='DD'
                      onChangeText={(lastVisitDay) => handlers.onlyNumbers('lastVisitDay', lastVisitDay)}
                      value={state.user.lastVisitDay}
                      keyboardType={state.component.inputPrimary.keyboardType.numeric}
                      enablesReturnKeyAutomatically={state.component.inputPrimary.enablesReturnKeyAuto}
                      maxLength={2}
                    />
                  </View>

                  <View>
                    <Text>/</Text>
                  </View>

                  <View style={{ width: (Dimensions.get('window').width / 4) }}>
                    <Input
                      ref={(input) => { this.lastVisitMonthRef = input }}
                      onSubmitEditing={() => { this.lastVisitYearRef.focus() }}
                      blurOnSubmit={false}
                      placeholder='MM'
                      onChangeText={(lastVisitMonth) => handlers.onlyNumbers('lastVisitMonth', lastVisitMonth)}
                      value={state.user.lastVisitMonth}
                      keyboardType={state.component.inputPrimary.keyboardType.numeric}
                      enablesReturnKeyAutomatically={state.component.inputPrimary.enablesReturnKeyAuto}
                      maxLength={2}
                    />
                  </View>

                  <View>
                    <Text>/</Text>
                  </View>

                  <View style={{ width: (Dimensions.get('window').width / 4) }}>
                    <Input
                      ref={(input) => { this.lastVisitYearRef = input }}
                      blurOnSubmit
                      placeholder='AAAA'
                      onChangeText={(lastVisitYear) => handlers.onlyNumbers('lastVisitYear', lastVisitYear)}
                      value={state.user.lastVisitYear}
                      keyboardType={state.component.inputPrimary.keyboardType.numeric}
                      enablesReturnKeyAutomatically={state.component.inputPrimary.enablesReturnKeyAuto}
                      maxLength={4}
                    />
                  </View>

                </View>
              </View>
              <View style={ComponentStyle.primaryButtonView}>
                <View>
                  <Button
                    raised
                    title='NÃO LEMBRO'
                    disabled={state.component.buttonSecondary.disabled}
                    onPress={() => handlers.onPress()}
                    buttonStyle={ComponentStyle.secondaryButton}
                  />
                </View>
                <View style={{ marginTop: 10 }}>
                  <Button
                    raised
                    title='ENVIAR'
                    disabled={state.component.buttonPrimary.disabled}
                    onPress={() => handlers.onPress()}
                    buttonStyle={ComponentStyle.primaryButton}
                  />
                </View>
              </View>
            </View>
        }
      </ScrollView>
    </KeyboardAwareScrollView>
  )
}

export default LastVisitScreen
