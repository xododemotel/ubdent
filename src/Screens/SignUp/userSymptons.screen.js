import React from 'react'
import { View } from 'react-native'
import VideoPlayerComponent from '../../Components/VideoPlayer/videoPlayer.component'
import useGlobal from '../../Store'
import AudioRecorder from '../../Components/AudioRecorder/audioRecorder.component'

export function UserSymptonsScreen ({ handlers }) {
  const [state] = useGlobal()

  return (
    <>
      <View>
        <VideoPlayerComponent handlers={handlers} />
      </View>
      <AudioRecorder
        fileName={state.user.document}
        controlsVisible={!state.component.buttonPrimary.disabled}
        buttonDisabled={state.component.buttonPrimary.disabled}
        handlers={handlers}
      />
    </>
  )
}

export default UserSymptonsScreen
