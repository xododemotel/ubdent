import React from 'react'
import { View, Image, Dimensions } from 'react-native'
import { Button } from 'react-native-elements'
import useGlobal from '../../Store'
import ComponentStyle from '../../Styles/component.style'

export function ShowMouthBottomPictureScreen ({ handlers }) {
  const [state] = useGlobal()
  console.log(state.anamnese.smilePicture)
  return (
    <View style={{ flex: 1 }}>
      <View style={{ flex: 5 }}>
        {
          (state.anamnese.mouthBottomPicture !== null) &&
            <Image
              source={{ uri: state.anamnese.mouthBottomPicture, isStatic: true }}
              style={{ width: Dimensions.get('window').width, height: 340 }}
            />
        }
      </View>
      <View style={ComponentStyle.primaryButtonView}>
        <View>
          <Button
            raised
            title='VOLTAR'
            onPress={() => handlers.onPress('TakeMouthBottomPicture')}
            buttonStyle={ComponentStyle.secondaryButton}
          />
        </View>
        <View style={{ marginTop: 10 }}>
          <Button
            raised
            title='CONTINUAR'
            onPress={() => handlers.uploadFile()}
            buttonStyle={ComponentStyle.primaryButton}
          />
        </View>
      </View>
    </View>
  )
}

export default ShowMouthBottomPictureScreen
