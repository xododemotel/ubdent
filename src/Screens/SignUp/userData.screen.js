import React from 'react'
import { View, ScrollView, Text, Dimensions } from 'react-native'
import useGlobal from '../../Store'
import { Input, Button } from 'react-native-elements'
// import { Dropdown } from 'react-native-material-dropdown'
import PositionStyle from '../../Styles/position.style'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import VideoPlayerComponent from '../../Components/VideoPlayer/videoPlayer.component'
import ComponentStyle from '../../Styles/component.style'

export function UserDataScreen ({ handlers }) {
  const [state] = useGlobal()

  return (
    <KeyboardAwareScrollView>
      <ScrollView contentContainerStyle={{ flexGrow: 1, height: Dimensions.get('window').height }} keyboardShouldPersistTaps='handled'>

        <View style={{ flex: 1 }}>
          <VideoPlayerComponent handlers={handlers} />
        </View>

        {
          (state.component.inputPrimary.visible) &&
            <View style={{ flex: 3 }}>
              <View style={{ flex: 7, marginLeft: (Dimensions.get('window').width * 0.05), marginRight: (Dimensions.get('window').width * 0.05) }}>
                <View style={PositionStyle.marginBottomBig}>
                  <Text>WhatsApp</Text>
                  <Input
                    onSubmitEditing={() => { this.emailRef.focus() }}
                    blurOnSubmit={false}
                    autoFocus={state.component.inputPrimary.autoFocus}
                    onChangeText={(phone) => handlers.maskPhone(phone)}
                    value={state.user.phone}
                    keyboardType={state.component.inputPrimary.keyboardType.phone}
                    enablesReturnKeyAutomatically={state.component.inputPrimary.enablesReturnKeyAuto}
                    returnKeyType='done'
                    maxLength={19}
                  />
                </View>

                <View style={PositionStyle.marginBottomBig}>
                  <Text>E-mail</Text>
                  <Input
                    ref={(input) => { this.emailRef = input }}
                    onSubmitEditing={() => { this.birthDayRef.focus() }}
                    blurOnSubmit={false}
                    onChangeText={(email) => handlers.onChangeText('email', email)}
                    value={state.user.email}
                    keyboardType={state.component.inputPrimary.keyboardType.email}
                    autoCapitalize='none'
                    enablesReturnKeyAutomatically={state.component.inputPrimary.enablesReturnKeyAuto}
                    returnKeyType='done'
                  />
                </View>

                <Text>Dia do seu aniversário</Text>

                <View style={[PositionStyle.marginBottomBig, { flexDirection: 'row' }]}>
                  <View style={{ width: (Dimensions.get('window').width / 4) }}>
                    <Input
                      ref={(input) => { this.birthDayRef = input }}
                      onSubmitEditing={() => { this.birthMonthRef.focus() }}
                      blurOnSubmit={false}
                      placeholder='DD'
                      onChangeText={(birthDay) => handlers.onlyNumbers('birthDay', birthDay)}
                      value={state.user.birthDay}
                      keyboardType={state.component.inputPrimary.keyboardType.numeric}
                      enablesReturnKeyAutomatically={state.component.inputPrimary.enablesReturnKeyAuto}
                      maxLength={2}
                      returnKeyType='done'
                    />
                  </View>

                  <View>
                    <Text>/</Text>
                  </View>

                  <View style={{ width: (Dimensions.get('window').width / 4) }}>
                    <Input
                      ref={(input) => { this.birthMonthRef = input }}
                      onSubmitEditing={() => { this.birthYearRef.focus() }}
                      blurOnSubmit={false}
                      placeholder='MM'
                      onChangeText={(birthMonth) => handlers.onlyNumbers('birthMonth', birthMonth)}
                      value={state.user.birthMonth}
                      keyboardType={state.component.inputPrimary.keyboardType.numeric}
                      enablesReturnKeyAutomatically={state.component.inputPrimary.enablesReturnKeyAuto}
                      maxLength={2}
                      returnKeyType='done'
                    />
                  </View>

                  <View>
                    <Text>/</Text>
                  </View>

                  <View style={{ width: (Dimensions.get('window').width / 4) }}>
                    <Input
                      ref={(input) => { this.birthYearRef = input }}
                      onSubmitEditing={handlers.maskBirthDate()}
                      blurOnSubmit={false}
                      placeholder='AAAA'
                      onChangeText={(birthYear) => handlers.onlyNumbers('birthYear', birthYear)}
                      value={state.user.birthYear}
                      keyboardType={state.component.inputPrimary.keyboardType.numeric}
                      enablesReturnKeyAutomatically={state.component.inputPrimary.enablesReturnKeyAuto}
                      maxLength={4}
                      returnKeyType='done'
                    />
                  </View>

                </View>

                <View style={PositionStyle.marginBottomBig}>
                  <Text>CPF</Text>
                  <Input
                    ref={(input) => { this.documentRef = input }}
                    blurOnSubmit
                    enablesReturnKeyAutomatically={state.component.inputPrimary.enablesReturnKeyAuto}
                    onChangeText={(document) => handlers.maskCPF(document)}
                    value={state.user.document}
                    keyboardType={state.component.inputPrimary.keyboardType.numeric}
                    maxLength={14}
                    returnKeyType='done'
                  />
                </View>

                <View style={PositionStyle.marginBottomBig}>
                  {/* <Dropdown
                    label='Sexo'
                    data={[
                      {
                        value: 'male',
                        label: 'Macho'
                      },
                      {
                        value: 'female',
                        label: 'Vadia'
                      },
                      {
                        value: 'undefined',
                        label: 'Não sabe o que quer da vida'
                      }
                    ]}
                    value={state.user.gender}
                    onChangeText={(gender) => handlers.onChangeText('gender', gender)}
                    dropdownPosition={0}
                    animationDuration={150}
                  /> */}
                </View>
              </View>
              <View style={{ flex: 2 }}>
                <View style={ComponentStyle.primaryButtonView}>
                  <Button
                    raised
                    title='ENVIAR'
                    disabled={state.component.buttonPrimary.disabled}
                    onPress={() => handlers.onPress()}
                    buttonStyle={ComponentStyle.primaryButton}
                  />
                </View>
              </View>
            </View>
        }
      </ScrollView>
    </KeyboardAwareScrollView>
  )
}

export default UserDataScreen
