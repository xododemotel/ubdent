import React from 'react'
import { View, ScrollView, Text, Dimensions } from 'react-native'
import useGlobal from '../../Store'
import { Input, Button } from 'react-native-elements'
import PositionStyle from '../../Styles/position.style'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import VideoPlayerComponent from '../../Components/VideoPlayer/videoPlayer.component'
import ComponentStyle from '../../Styles/component.style'

export function DoctorLastVisitScreen ({ handlers }) {
  const [state] = useGlobal()

  return (
    <KeyboardAwareScrollView>
      <ScrollView contentContainerStyle={{ flexGrow: 1, height: Dimensions.get('window').height }} keyboardShouldPersistTaps='handled'>

        <View style={{ flex: 1 }}>
          <VideoPlayerComponent handlers={handlers} />
        </View>

        {
          (state.component.inputPrimary.visible) &&
            <View style={{ flex: 3 }}>
              <View style={{ flex: 2, marginLeft: (Dimensions.get('window').width * 0.05), marginRight: (Dimensions.get('window').width * 0.05) }}>

                <Text>Data da última visita ao médico</Text>

                <View style={[PositionStyle.marginBottomBig, { flexDirection: 'row' }]}>
                  <View style={{ width: (Dimensions.get('window').width / 4) }}>
                    <Input
                      ref={(input) => { this.doctorLastVisitDayRef = input }}
                      onSubmitEditing={() => { this.doctorLastVisitMonthRef.focus() }}
                      blurOnSubmit={false}
                      placeholder='DD'
                      onChangeText={(doctorLastVisitDay) => handlers.onlyNumbers('doctorLastVisitDay', doctorLastVisitDay)}
                      value={state.user.doctorLastVisitDay}
                      keyboardType={state.component.inputPrimary.keyboardType.numeric}
                      enablesReturnKeyAutomatically={state.component.inputPrimary.enablesReturnKeyAuto}
                      maxLength={2}
                    />
                  </View>

                  <View>
                    <Text>/</Text>
                  </View>

                  <View style={{ width: (Dimensions.get('window').width / 4) }}>
                    <Input
                      ref={(input) => { this.doctorLastVisitMonthRef = input }}
                      onSubmitEditing={() => { this.doctorLastVisitYearRef.focus() }}
                      blurOnSubmit={false}
                      placeholder='MM'
                      onChangeText={(doctorLastVisitMonth) => handlers.onlyNumbers('doctorLastVisitMonth', doctorLastVisitMonth)}
                      value={state.user.doctorLastVisitMonth}
                      keyboardType={state.component.inputPrimary.keyboardType.numeric}
                      enablesReturnKeyAutomatically={state.component.inputPrimary.enablesReturnKeyAuto}
                      maxLength={2}
                    />
                  </View>

                  <View>
                    <Text>/</Text>
                  </View>

                  <View style={{ width: (Dimensions.get('window').width / 4) }}>
                    <Input
                      ref={(input) => { this.doctorLastVisitYearRef = input }}
                      blurOnSubmit
                      placeholder='AAAA'
                      onChangeText={(doctorLastVisitYear) => handlers.onlyNumbers('doctorLastVisitYear', doctorLastVisitYear)}
                      value={state.user.doctorLastVisitYear}
                      keyboardType={state.component.inputPrimary.keyboardType.numeric}
                      enablesReturnKeyAutomatically={state.component.inputPrimary.enablesReturnKeyAuto}
                      maxLength={4}
                    />
                  </View>

                </View>
              </View>
              <View style={ComponentStyle.primaryButtonView}>
                <View>
                  <Button
                    raised
                    title='NÃO LEMBRO'
                    disabled={state.component.buttonSecondary.disabled}
                    onPress={() => handlers.onPress()}
                    buttonStyle={ComponentStyle.secondaryButton}
                  />
                </View>
                <View style={{ marginTop: 10 }}>
                  <Button
                    raised
                    title='ENVIAR'
                    disabled={state.component.buttonPrimary.disabled}
                    onPress={() => handlers.onPress()}
                    buttonStyle={ComponentStyle.primaryButton}
                  />
                </View>
              </View>
            </View>
        }
      </ScrollView>
    </KeyboardAwareScrollView>
  )
}

export default DoctorLastVisitScreen
