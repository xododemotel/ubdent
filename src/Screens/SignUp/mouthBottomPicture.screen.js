import React from 'react'
import { View, Image, Dimensions } from 'react-native'
import VideoPlayerComponent from '../../Components/VideoPlayer/videoPlayer.component'
import useGlobal from '../../Store'
import { Button } from 'react-native-elements'
import ComponentStyle from '../../Styles/component.style'

export function MouthTopPictureScreen ({ handlers }) {
  const [state] = useGlobal()

  return (
    <View style={{ flex: 1 }}>
      <View>
        <VideoPlayerComponent handlers={handlers} />
      </View>
      <View style={{ flex: 1 }}>
        {
          (!state.component.buttonPrimary.disabled) &&
            <>
              <View style={{ flex: 6 }}>
                <Image
                  style={{ width: Dimensions.get('window').width, height: 240 }}
                  source={require('../../Assets/images/mouth_bottom_picture.png')}
                />
              </View>
              <View style={ComponentStyle.primaryButtonView}>
                <Button
                  raised
                  title='TIRAR TERCEIRA FOTO'
                  disabled={state.component.buttonPrimary.disabled}
                  onPress={() => handlers.onPress()}
                  buttonStyle={ComponentStyle.primaryButton}
                />
              </View>
            </>
        }
      </View>
    </View>
  )
}

export default MouthTopPictureScreen
