import React from 'react'
import { View, Switch, ScrollView, Dimensions, Text } from 'react-native'
import VideoPlayerComponent from '../../Components/VideoPlayer/videoPlayer.component'
import useGlobal from '../../Store'
import { Input, Button, Icon } from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ComponentStyle from '../../Styles/component.style'

export function TakeMedicationScreen ({ handlers }) {
  const [state] = useGlobal()

  return (
    <KeyboardAwareScrollView>
      <ScrollView contentContainerStyle={{ flexGrow: 1, height: Dimensions.get('window').height }} keyboardShouldPersistTaps='handled'>

        <View style={{ flex: 1 }}>
          <VideoPlayerComponent handlers={handlers} />
        </View>
        <View style={{ flex: 3 }}>
          {
            (!state.component.buttonPrimary.disabled) &&
              <View style={{ flex: 1 }}>
                <View style={{ flex: 4 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <Icon name='sentiment-satisfied' size={32} />
                    <Switch
                      onValueChange={() => handlers.onChangeText('takeMedication', !state.anamnese.takeMedication)}
                      value={state.anamnese.takeMedication}
                      trackColor={{ false: 'green', true: 'red' }}
                    />
                    <Icon name='sentiment-dissatisfied' size={32} />
                  </View>
                  <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <View style={{ marginTop: 20 }}>
                      {(state.anamnese.takeMedication) && <Text>Sim</Text>}
                      {(!state.anamnese.takeMedication) && <Text>Não</Text>}
                    </View>
                  </View>
                  {
                    (state.anamnese.takeMedication) &&
                      <View>
                        <Input
                          autoFocus={state.component.inputPrimary.autoFocus}
                          onChangeText={(takeMedicationDetails) => handlers.onChangeText('takeMedicationDetails', takeMedicationDetails)}
                          value={state.anamnese.takeMedicationDetails}
                          multiline
                          numberOfLines={5}
                          label='Detalhes'
                        />
                      </View>
                  }
                </View>
                <View style={ComponentStyle.primaryButtonView}>
                  <Button
                    raised
                    title='CONTINUAR'
                    disabled={state.component.buttonPrimary.disabled}
                    onPress={() => handlers.onPress()}
                    buttonStyle={ComponentStyle.secondaryButton}
                  />
                </View>
              </View>
          }
        </View>
      </ScrollView>
    </KeyboardAwareScrollView>
  )
}

export default TakeMedicationScreen
