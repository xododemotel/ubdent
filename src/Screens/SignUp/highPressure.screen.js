import React from 'react'
import { View, Switch, Text } from 'react-native'
import VideoPlayerComponent from '../../Components/VideoPlayer/videoPlayer.component'
import useGlobal from '../../Store'
import { Button, Icon } from 'react-native-elements'
import ComponentStyle from '../../Styles/component.style'

export function HighPressureScreen ({ handlers }) {
  const [state] = useGlobal()

  return (
    <>
      <View style={{ flex: 1 }}>
        <VideoPlayerComponent handlers={handlers} />
      </View>
      <View style={{ flex: 3 }}>
        {
          (!state.component.buttonPrimary.disabled) &&
            <>
              <View style={{ flex: 5 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', margin: 10 }}>
                  <Switch
                    onValueChange={() => handlers.onChangeText('lowPressure', !state.anamnese.lowPressure)}
                    value={state.anamnese.lowPressure}
                    trackColor={{ true: 'yellow' }}
                  />
                  <Icon name='vertical-align-bottom' size={32} />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', margin: 10 }}>
                  <Switch
                    onValueChange={() => handlers.onChangeText('normalPressure', !state.anamnese.normalPressure)}
                    value={state.anamnese.normalPressure}
                    color
                  />
                  <Icon name='vertical-align-center' size={32} />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <Switch
                    onValueChange={() => handlers.onChangeText('highPressure', !state.anamnese.highPressure)}
                    value={state.anamnese.highPressure}
                    trackColor={{ true: 'red' }}
                  />
                  <Icon name='vertical-align-top' size={32} />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <View style={{ marginTop: 20 }}>
                    {(state.anamnese.lowPressure) && <Text>Baixa</Text>}
                    {(state.anamnese.normalPressure) && <Text>Normal</Text>}
                    {(state.anamnese.highPressure) && <Text>Alta</Text>}
                  </View>
                </View>
              </View>
              <View style={ComponentStyle.primaryButtonView}>
                <Button
                  raised
                  title='CONTINUAR'
                  disabled={state.component.buttonPrimary.disabled}
                  onPress={() => handlers.onPress()}
                  buttonStyle={ComponentStyle.primaryButton}
                />
              </View>
            </>
        }
      </View>
    </>
  )
}

export default HighPressureScreen
