import React from 'react'
import { View, Switch, Text } from 'react-native'
import VideoPlayerComponent from '../../Components/VideoPlayer/videoPlayer.component'
import useGlobal from '../../Store'
import { Button, Icon } from 'react-native-elements'
import ComponentStyle from '../../Styles/component.style'

export function DentistFearScreen ({ handlers }) {
  const [state] = useGlobal()

  return (
    <View style={{ flex: 1 }}>
      <View style={{ flex: 1 }}>
        <VideoPlayerComponent handlers={handlers} />
      </View>
      <View style={{ flex: 3 }}>
        {
          (!state.component.buttonPrimary.disabled) &&
            <View style={{ flex: 1 }}>
              <View style={{ flex: 7 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <Icon name='sentiment-satisfied' size={32} />
                  <Switch
                    onValueChange={() => handlers.onChangeText('dentistFear', !state.anamnese.dentistFear)}
                    value={state.anamnese.dentistFear}
                    trackColor={{ false: 'green', true: 'red' }}
                  />
                  <Icon name='sentiment-dissatisfied' size={32} />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <View style={{ marginTop: 20 }}>
                    {(state.anamnese.dentistFear) && <Text>Sim</Text>}
                    {(!state.anamnese.dentistFear) && <Text>Não</Text>}
                  </View>
                </View>
              </View>
              <View style={ComponentStyle.primaryButtonView}>
                <Button
                  raised
                  title='CONTINUAR'
                  disabled={state.component.buttonPrimary.disabled}
                  onPress={() => handlers.onPress()}
                  buttonStyle={ComponentStyle.primaryButton}
                />
              </View>
            </View>
        }
      </View>
    </View>
  )
}

export default DentistFearScreen
