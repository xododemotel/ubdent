import { StyleSheet } from 'react-native'
import ConfigStyle from './config.style'

const { color } = ConfigStyle

const ColorStyle = StyleSheet.create({
  white: {
    color: color.white
  },
  black: {
    color: color.black
  },
  backgroundWhite: {
    backgroundColor: color.white
  },
  backgroundBlack: {
    backgroundColor: color.black
  }
})

export default ColorStyle
