import { StyleSheet, Dimensions } from 'react-native'

const ComponentStyle = StyleSheet.create({
  picker: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30
  },
  primaryButtonView: {
    flex: 1,
    width: (Dimensions.get('window').width * 0.9),
    marginLeft: (Dimensions.get('window').width * 0.05),
    marginRight: (Dimensions.get('window').width * 0.05)
  },
  primaryButton: {
    borderRadius: 25,
    backgroundColor: '#159193'
  },
  secondaryButton: {
    borderRadius: 25,
    backgroundColor: '#3887af'
  }
})

export default ComponentStyle
