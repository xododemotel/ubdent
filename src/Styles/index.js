import { StyleSheet, Dimensions, Platform } from 'react-native'
import ColorStyle from './color.style'
import PositionStyle from './position.style'

const Styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    height: Dimensions.get('window').height,
    paddingTop: (Platform.OS === 'ios') ? 50 : 24,
    ...ColorStyle.backgroundWhite,
    ...PositionStyle.justifyContentCenter
  }
})

export default Styles
