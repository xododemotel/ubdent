const ConfigStyle = {
  color: {
    white: '#FFFFFF',
    black: '#000000'
  },
  position: {
    center: 'center',
    left: 'left',
    right: 'right',
    bottom: 'bottom',
    top: 'top'
  },
  margin: {
    extraSmall: 3,
    small: 5,
    regular: 8,
    big: 10,
    extraBig: 12
  },
  padding: {
    extraSmall: 3,
    small: 5,
    regular: 8,
    big: 10,
    extraBig: 12
  }
}

export default ConfigStyle
