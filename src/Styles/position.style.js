import { StyleSheet } from 'react-native'
import ConfigStyle from './config.style'

const { position, margin } = ConfigStyle

const PositionStyle = StyleSheet.create({
  justifyContentCenter: {
    justifyContent: position.center
  },
  marginExtraSmall: {
    margin: margin.extraSmall
  },
  marginBottomExtraSmall: {
    marginBottom: margin.extraSmall
  },
  marginBottomBig: {
    marginBottom: margin.big
  },
  marginTopExtraSmall: {
    marginTop: margin.extraSmall
  },
  marginRightExtraSmall: {
    marginRight: margin.extraSmall
  },
  marginLeftExtraSmall: {
    marginRight: margin.extraSmall
  },
  marginSmall: {
    margin: margin.small
  },
  marginRegular: {
    margin: margin.regular
  }
})

export default PositionStyle
