import { createStackNavigator, createAppContainer } from 'react-navigation'
import ForkController from '../Controllers/Fork/fork.controller'
import MainController from '../Controllers/SignUp/main.controller'
import UserDataController from '../Controllers/SignUp/userData.controller'
import UserSymptonsController from '../Controllers/SignUp/userSymptons.controller'
import SmilePictureController from '../Controllers/SignUp/smilePicture.controller'
import SmilePictureExampleController from '../Controllers/SignUp/smilePictureExample.controller'
import TakeSmilePictureController from '../Controllers/SignUp/takeSmilePicture.controller'
import ShowSmilePictureController from '../Controllers/SignUp/showSmilePicture.controller'
import MouthTopPictureController from '../Controllers/SignUp/mouthTopPicture.controller'
import TakeMouthTopPictureController from '../Controllers/SignUp/takeMouthTopPicture.controller'
import ShowMouthTopPictureController from '../Controllers/SignUp/showMouthTopPicture.controller'
import MouthBottomPictureController from '../Controllers/SignUp/mouthBottomPicture.controller'
import TakeMouthBottomPictureController from '../Controllers/SignUp/takeMouthBottomPicture.controller'
import ShowMouthBottomPictureController from '../Controllers/SignUp/showMouthBottomPicture.controller'
import FinishController from '../Controllers/SignUp/finish.controller'
import LastVisitController from '../Controllers/SignUp/lastVisit.controller'
import DentistFearController from '../Controllers/SignUp/dentistFear.controller'
import DoctorLastVisitController from '../Controllers/SignUp/doctorLastVisit.controller'
import HealthProblemsController from '../Controllers/SignUp/healthProblems.controller'
import TakeMedicationController from '../Controllers/SignUp/takeMedication.controller'
import HaveAllergyController from '../Controllers/SignUp/haveAllergy.controller'
import HighPressureControler from '../Controllers/SignUp/highPressure.controller'
import DiabetesController from '../Controllers/SignUp/diabetes.controller'
import HearthProblemController from '../Controllers/SignUp/hearthProblem.controller'
import PregnantController from '../Controllers/SignUp/pregnant.controller'

const AppNavigator = createStackNavigator({
  Fork: {
    screen: ForkController
  },
  Main: {
    screen: MainController
  },
  UserData: {
    screen: UserDataController
  },
  UserSymptons: {
    screen: UserSymptonsController
  },
  SmilePicture: {
    screen: SmilePictureController
  },
  SmilePictureExample: {
    screen: SmilePictureExampleController
  },
  TakeSmilePicture: {
    screen: TakeSmilePictureController
  },
  ShowSmilePicture: {
    screen: ShowSmilePictureController
  },
  MouthTopPicture: {
    screen: MouthTopPictureController
  },
  TakeMouthTopPicture: {
    screen: TakeMouthTopPictureController
  },
  ShowMouthTopPicture: {
    screen: ShowMouthTopPictureController
  },
  MouthBottomPicture: {
    screen: MouthBottomPictureController
  },
  TakeMouthBottomPicture: {
    screen: TakeMouthBottomPictureController
  },
  ShowMouthBottomPicture: {
    screen: ShowMouthBottomPictureController
  },
  LastVisit: {
    screen: LastVisitController
  },
  DentistFear: {
    screen: DentistFearController
  },
  DoctorLastVisit: {
    screen: DoctorLastVisitController
  },
  HealthProblems: {
    screen: HealthProblemsController
  },
  TakeMedication: {
    screen: TakeMedicationController
  },
  HaveAllergy: {
    screen: HaveAllergyController
  },
  HighPressure: {
    screen: HighPressureControler
  },
  Diabetes: {
    screen: DiabetesController
  },
  HearthProblem: {
    screen: HearthProblemController
  },
  Pregnant: {
    screen: PregnantController
  },
  Finish: {
    screen: FinishController
  }
})

export default createAppContainer(AppNavigator)
