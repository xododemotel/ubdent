/**
 * Ubdent App by Ubdent
 * Created by: Leandro Maciel
 * Version: 0.1.0
 */

import React, { useEffect } from 'react'
import Navigator from './Navigator/index'
import useGlobal from './Store'

function App () {
  const [, actions] = useGlobal()

  useEffect(() => {
    actions.connect()
  }, [])

  return (
    <Navigator />
  )
}

export default App
