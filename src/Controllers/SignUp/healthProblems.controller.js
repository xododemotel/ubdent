import React, { useEffect } from 'react'
import { View } from 'react-native'
import HealthProblemsScreen from '../../Screens/SignUp/healthProblems.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function HealthProblemsController () {
  const [state, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
      actions.updateComponentState('inputPrimary', 'visible', false)
      actions.updateComponentState('inputPrimary', 'autofocus', false)
    } catch (error) {
      console.log(error)
    }
  }, [])

  useEffect(() => {
    try {
      if (state.anamnese.healthProblems) {
        actions.updateComponentState('inputPrimary', 'visible', true)
        actions.updateComponentState('inputPrimary', 'autofocus', true)
      } else {
        actions.updateComponentState('inputPrimary', 'visible', false)
      }
    } catch (error) {
      console.log(error)
    }
  }, [state.anamnese.healthProblems])

  const handleOnChangeText = (property, value) => {
    try {
      actions.updateAnamneseState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      resetStackNavigation(0, ['TakeMedication'], navigate)
      navigator.navigate('')
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onChangeText: handleOnChangeText,
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    video: '11-problema-de-saude'
  }

  return (
    <View style={Styles.mainContainer}>
      <HealthProblemsScreen handlers={handlers} />
    </View>
  )
}

HealthProblemsController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default HealthProblemsController
