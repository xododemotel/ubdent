import React, { useEffect } from 'react'
import { View } from 'react-native'
import DiabetesScreen from '../../Screens/SignUp/diabetes.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function DiabetesController () {
  const [, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
      actions.updateComponentState('inputPrimary', 'visible', false)
    } catch (error) {
      console.log(error)
    }
  }, [])

  const handleOnChangeText = (property, value) => {
    try {
      actions.updateAnamneseState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      resetStackNavigation(0, ['HearthProblem'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onChangeText: handleOnChangeText,
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    video: '15-diabetes'
  }

  return (
    <View style={Styles.mainContainer}>
      <DiabetesScreen handlers={handlers} />
    </View>
  )
}

DiabetesController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default DiabetesController
