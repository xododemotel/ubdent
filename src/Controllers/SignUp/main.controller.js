import React, { useEffect } from 'react'
import { View, Keyboard } from 'react-native'
import MainScreen from '../../Screens/SignUp/main.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function MainController () {
  const [state, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
      actions.updateComponentState('inputPrimary', 'visible', false)
      actions.updateComponentState('inputPrimary', 'autofocus', false)
    } catch (error) {
      console.log(error)
    }
  }, [])

  useEffect(() => {
    try {
      if (state.user.name !== null && state.component.buttonPrimary.disabled === true) {
        actions.updateComponentState('buttonPrimary', 'disabled', false)
        actions.updateComponentState('inputPrimary', 'visible', true)
        actions.updateComponentState('inputPrimary', 'autofocus', true)
      }
      if (state.user.name === '' || state.user.name === null) {
        actions.updateComponentState('buttonPrimary', 'disabled', true)
      }
    } catch (error) {
      console.log(error)
    }
  }, [state.user.name])

  const handleOnChangeText = (property, value) => {
    try {
      actions.updateUserState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('inputPrimary', 'visible', true)
      actions.updateComponentState('inputPrimary', 'autofocus', true)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      Keyboard.dismiss()
      actions.updateComponentState('buttonPrimary', 'disabled', true)
      actions.updateComponentState('inputPrimary', 'visible', false)
      resetStackNavigation(0, ['UserData'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onChangeText: handleOnChangeText,
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    video: '1-nome-completo'
  }

  return (
    <View style={Styles.mainContainer}>
      <MainScreen handlers={handlers} />
    </View>
  )
}

MainController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default MainController
