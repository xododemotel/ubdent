import React, { useEffect } from 'react'
import { View } from 'react-native'
import DentistFearScreen from '../../Screens/SignUp/dentistFear.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function DentistFearController () {
  const [, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
    } catch (error) {
      console.log(error)
    }
  }, [])

  const handleOnChangeText = (property, value) => {
    try {
      actions.updateAnamneseState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      resetStackNavigation(0, ['DoctorLastVisit'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onChangeText: handleOnChangeText,
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    video: '9-medo-de-dentista'
  }

  return (
    <View style={Styles.mainContainer}>
      <DentistFearScreen handlers={handlers} />
    </View>
  )
}

DentistFearController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default DentistFearController
