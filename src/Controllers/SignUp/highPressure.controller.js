import React, { useEffect } from 'react'
import { View } from 'react-native'
import HighPressureScreen from '../../Screens/SignUp/highPressure.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function HighPressureControler () {
  const [, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
    } catch (error) {
      console.log(error)
    }
  }, [])

  const handleOnChangeText = (property, value) => {
    try {
      switch (property) {
        case 'lowPressure':
          actions.updateAnamneseState('normalPressure', false)
          actions.updateAnamneseState('highPressure', false)
          break
        case 'normalPressure':
          actions.updateAnamneseState('lowPressure', false)
          actions.updateAnamneseState('highPressure', false)
          break
        case 'highPressure':
          actions.updateAnamneseState('lowPressure', false)
          actions.updateAnamneseState('normalPressure', false)
          break
      }
      actions.updateAnamneseState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      resetStackNavigation(0, ['Diabetes'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onChangeText: handleOnChangeText,
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    video: '14-pressao-alta'
  }

  return (
    <View style={Styles.mainContainer}>
      <HighPressureScreen handlers={handlers} />
    </View>
  )
}

HighPressureControler.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default HighPressureControler
