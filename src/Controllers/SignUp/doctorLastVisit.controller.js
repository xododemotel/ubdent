import React, { useEffect } from 'react'
import { View } from 'react-native'
import DoctorLastVisitScreen from '../../Screens/SignUp/doctorLastVisit.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function DoctorLastVisitController () {
  const [state, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
      actions.updateComponentState('buttonSecondary', 'disabled', true)
      actions.updateComponentState('inputPrimary', 'visible', false)
      actions.updateComponentState('inputPrimary', 'autofocus', false)
    } catch (error) {
      console.log(error)
    }
  }, [])

  useEffect(() => {
    if (state.anamnese.doctorLastVisitDay.length === 2) {
      this.doctorLastVisitMonthRef.focus()
    }
  }, [state.anamnese.doctorLastVisitDay.length])

  useEffect(() => {
    if (state.anamnese.doctorLastVisitMonth.length === 2) {
      this.doctorLastVisitYearRef.focus()
    }
  }, [state.anamnese.doctorLastVisitMonth.length])

  useEffect(() => {
    if (state.anamnese.doctorLastVisitDay.length === 2 && state.anamnese.doctorLastVisitMonth.length === 2 && state.anamnese.doctorLastVisitYear.length === 4) {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } else {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
    }
  }, [state.anamnese.doctorLastVisitDay.length, state.anamnese.doctorLastVisitMonth.length, state.anamnese.doctorLastVisitYear.length])

  const handleOnChangeText = (property, value) => {
    try {
      actions.updateAnamneseState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('inputPrimary', 'visible', true)
      actions.updateComponentState('buttonSecondary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      resetStackNavigation(0, ['HealthProblems'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnlyNumbers = (property, value) => {
    try {
      value = value.replace(/\D/g, '')

      actions.updateAnamneseState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onChangeText: handleOnChangeText,
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    onlyNumbers: handleOnlyNumbers,
    video: '10-ultima-ida-ao-medico'
  }

  return (
    <View style={Styles.mainContainer}>
      <DoctorLastVisitScreen handlers={handlers} />
    </View>
  )
}

DoctorLastVisitController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default DoctorLastVisitController
