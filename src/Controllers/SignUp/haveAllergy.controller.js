import React, { useEffect } from 'react'
import { View } from 'react-native'
import HaveAllergyScreen from '../../Screens/SignUp/haveAllergy.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function HaveAllergyController () {
  const [state, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
      actions.updateComponentState('inputPrimary', 'visible', false)
      actions.updateComponentState('inputPrimary', 'autofocus', false)
    } catch (error) {
      console.log(error)
    }
  }, [])

  useEffect(() => {
    try {
      if (state.anamnese.haveAllergy) {
        actions.updateComponentState('inputPrimary', 'visible', true)
        actions.updateComponentState('inputPrimary', 'autofocus', true)
      } else {
        actions.updateComponentState('inputPrimary', 'visible', false)
      }
    } catch (error) {
      console.log(error)
    }
  }, [state.anamnese.haveAllergy])

  const handleOnChangeText = (property, value) => {
    try {
      actions.updateAnamneseState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      resetStackNavigation(0, ['HighPressure'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onChangeText: handleOnChangeText,
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    video: '13-possui-alergia'
  }

  return (
    <View style={Styles.mainContainer}>
      <HaveAllergyScreen handlers={handlers} />
    </View>
  )
}

HaveAllergyController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default HaveAllergyController
