import React, { useEffect } from 'react'
import { View, Alert } from 'react-native'
import PregnantScreen from '../../Screens/SignUp/pregnant.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function PregnantController () {
  const [, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
      actions.updateComponentState('inputPrimary', 'visible', false)
    } catch (error) {
      console.log(error)
    }
  }, [])

  const handleOnChangeSwitch = (property, value) => {
    try {
      actions.updateAnamneseState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      const [error, response] = await actions.addMedicalData()
      if (response) {
        resetStackNavigation(0, ['Finish'], navigate)
      } else {
        console.log(error)
        Alert.alert(
          'OPS',
          'Houve um erro ao enviar seus dados. Por favor tente novamente mais tarde.',
          [
            {
              text: 'OK',
              onPress: () => {
                actions.clearUserState()
                resetStackNavigation(0, ['Fork'], navigate)
              }
            }
          ],
          { cancelable: false }
        )
      }
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onChangeText: handleOnChangeSwitch,
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    video: '17-gravida'
  }

  return (
    <View style={Styles.mainContainer}>
      <PregnantScreen handlers={handlers} />
    </View>
  )
}

PregnantController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default PregnantController
