import React, { useEffect } from 'react'
import { View } from 'react-native'
import LastVisitScreen from '../../Screens/SignUp/lastVisit.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function LastVisitController () {
  const [state, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
      actions.updateComponentState('buttonSecondary', 'disabled', true)
      actions.updateComponentState('inputPrimary', 'visible', false)
      actions.updateComponentState('inputPrimary', 'autofocus', false)
    } catch (error) {
      console.log(error)
    }
  }, [])

  useEffect(() => {
    if (state.anamnese.lastVisitDay.length === 2) {
      this.lastVisitMonthRef.focus()
    }
  }, [state.anamnese.lastVisitDay.length])

  useEffect(() => {
    if (state.anamnese.lastVisitMonth.length === 2) {
      this.lastVisitYearRef.focus()
    }
  }, [state.anamnese.lastVisitMonth.length])

  useEffect(() => {
    if (state.anamnese.lastVisitDay.length === 2 && state.anamnese.lastVisitMonth.length === 2 && state.anamnese.lastVisitYear.length === 4) {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } else {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
    }
  }, [state.anamnese.lastVisitDay.length, state.anamnese.lastVisitMonth.length, state.anamnese.lastVisitYear.length])

  const handleOnChangeText = (property, value) => {
    try {
      actions.updateAnamneseState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('inputPrimary', 'visible', true)
      actions.updateComponentState('buttonSecondary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      resetStackNavigation(0, ['DentistFear'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnlyNumbers = (property, value) => {
    try {
      value = value.replace(/\D/g, '')

      actions.updateAnamneseState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onChangeText: handleOnChangeText,
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    onlyNumbers: handleOnlyNumbers,
    video: '8-ultima-visita-dentista'
  }

  return (
    <View style={Styles.mainContainer}>
      <LastVisitScreen handlers={handlers} />
    </View>
  )
}

LastVisitController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default LastVisitController
