import React from 'react'
import { View, Alert } from 'react-native'
import ShowSmilePictureScreen from '../../Screens/SignUp/showSmilePicture.screen'
import { useNavigation } from 'react-navigation-hooks'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { resetStackNavigation } from '../../Utils/navigation.util'

function ShowSmilePictureController () {
  const navigate = useNavigation()
  const [state, actions] = useGlobal()

  const handleNavigation = (destination) => {
    try {
      resetStackNavigation(0, [destination], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handleUploadFile = async () => {
    try {
      const [, response] = await actions.uploadFile(state.anamnese.smilePicture, 'smile')
      if (response) {
        handleNavigation('MouthTopPicture')
      } else {
        Alert.alert(
          'OPS',
          'Não foi possível enviar sua foto. Por favor, tire outra foto e tente novamente.',
          [
            { text: 'OK' }
          ],
          { cancelable: false }
        )
      }
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    uploadFile: handleUploadFile,
    onPress: handleNavigation
  }

  return (
    <View style={Styles.mainContainer}>
      <ShowSmilePictureScreen handlers={handlers} />
    </View>
  )
}

ShowSmilePictureController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default ShowSmilePictureController
