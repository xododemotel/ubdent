import React from 'react'
import { View } from 'react-native'
import VideoPlayerComponent from '../../Components/VideoPlayer/videoPlayer.component'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'
import useGlobal from '../../Store'

function FinishControler () {
  const navigate = useNavigation()
  const [, actions] = useGlobal()

  const handleOnEndVideo = () => {
    try {
      actions.clearUserState()
      resetStackNavigation(0, ['Fork'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onEndVideo: handleOnEndVideo,
    video: '18-agradecimento'
  }

  return (
    <View style={Styles.mainContainer}>
      <VideoPlayerComponent handlers={handlers} />
    </View>
  )
}

FinishControler.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default FinishControler
