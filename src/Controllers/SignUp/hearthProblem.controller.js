import React, { useEffect } from 'react'
import { View } from 'react-native'
import HeartProblemScreen from '../../Screens/SignUp/hearthProblem.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function HearthProblemController () {
  const [, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
      actions.updateComponentState('inputPrimary', 'visible', false)
    } catch (error) {
      console.log(error)
    }
  }, [])

  const handleOnChangeSwitch = (property, value) => {
    try {
      actions.updateAnamneseState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      resetStackNavigation(0, ['Pregnant'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onChangeText: handleOnChangeSwitch,
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    video: '16-problema-do-coracao'
  }

  return (
    <View style={Styles.mainContainer}>
      <HeartProblemScreen handlers={handlers} />
    </View>
  )
}

HearthProblemController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default HearthProblemController
