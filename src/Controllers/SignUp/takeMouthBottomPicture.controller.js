import React from 'react'
import useGlobal from '../../Store'
import { useNavigation } from 'react-navigation-hooks'
import TakeMouthBottomPictureScreen from '../../Screens/SignUp/takeMouthBottomPicture.screen'
import { resetStackNavigation } from '../../Utils/navigation.util'

function TakeMouthBottomPictureController () {
  const [, actions] = useGlobal()
  const navigate = useNavigation()

  const handleTakePicture = async (camera) => {
    if (camera) {
      const options = { quality: 0.5, base64: true }
      const data = await camera.takePictureAsync(options)
      if (data) {
        actions.updateAnamneseState('mouthBottomPicture', data.uri)
        resetStackNavigation(0, ['ShowMouthBottomPicture'], navigate)
      }
    }
  }

  const handlers = {
    takePicture: handleTakePicture
  }

  return <TakeMouthBottomPictureScreen handlers={handlers} />
}

TakeMouthBottomPictureController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default TakeMouthBottomPictureController
