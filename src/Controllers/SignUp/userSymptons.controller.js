import React, { useEffect } from 'react'
import { View } from 'react-native'
import UserSymptonsScreen from '../../Screens/SignUp/userSymptons.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'

function UserSymptonsController () {
  const [state, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
    } catch (error) {
      console.log(error)
    }
  }, [])

  useEffect(() => {
    try {
      if (state.anamnese.symptons !== null && state.component.buttonPrimary.disabled === true) {
        actions.updateComponentState('buttonPrimary', 'disabled', false)
      }
      if (state.anamnese.symptons === '' || state.anamnese.symptons === null) {
        actions.updateComponentState('buttonPrimary', { disabled: true })
      }
    } catch (error) {
      console.log(error)
    }
  }, [state.anamnese.symptons])

  const handleOnChangeText = (property, value) => {
    try {
      actions.updateAnamneseState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnEndVideo = (property, value) => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = () => {
    try {
      navigate.navigate('SmilePicture')
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onChangeText: handleOnChangeText,
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    video: '3-sintomas'
  }

  return (
    <View style={Styles.mainContainer}>
      <UserSymptonsScreen handlers={handlers} />
    </View>
  )
}

UserSymptonsController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default UserSymptonsController
