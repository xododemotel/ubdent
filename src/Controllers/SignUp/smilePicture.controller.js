import React, { useEffect } from 'react'
import { View } from 'react-native'
import useGlobal from '../../Store'
import SmilePictureScreen from '../../Screens/SignUp/smilePicture.screen'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function SmilePictureController () {
  const navigate = useNavigation()
  const [, actions] = useGlobal()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
    } catch (error) {
      console.log(error)
    }
  }, [])

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = () => {
    try {
      resetStackNavigation(0, ['SmilePictureExample'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    video: '4-foto-sorriso-frontal'
  }

  return (
    <View style={Styles.mainContainer}>
      <SmilePictureScreen handlers={handlers} />
    </View>
  )
}

SmilePictureController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default SmilePictureController
