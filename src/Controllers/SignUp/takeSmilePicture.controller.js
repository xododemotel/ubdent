import React from 'react'
import useGlobal from '../../Store'
import { useNavigation } from 'react-navigation-hooks'
import CameraComponent from '../../Components/Camera/camera.component'
import { resetStackNavigation } from '../../Utils/navigation.util'

function TakeSmilePictureController () {
  const [, actions] = useGlobal()
  const navigate = useNavigation()

  const handleTakePicture = async (camera) => {
    if (camera) {
      const options = { quality: 0.5, base64: true }
      const data = await camera.takePictureAsync(options)
      if (data) {
        actions.updateAnamneseState('smilePicture', data.uri)
        resetStackNavigation(0, ['ShowSmilePicture'], navigate)
      }
    }
  }

  const handlers = {
    takePicture: handleTakePicture
  }

  return <CameraComponent handlers={handlers} />
}

TakeSmilePictureController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default TakeSmilePictureController
