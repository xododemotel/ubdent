import React, { useEffect } from 'react'
import { View } from 'react-native'
import useGlobal from '../../Store'
import MouthTopPictureScreen from '../../Screens/SignUp/mouthTopPicture.screen'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function MouthTopPictureController () {
  const navigate = useNavigation()
  const [, actions] = useGlobal()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
    } catch (error) {
      console.log(error)
    }
  }, [])

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      resetStackNavigation(0, ['TakeMouthTopPicture'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    video: '6-foto-ceu-da-boca'
  }

  return (
    <View style={Styles.mainContainer}>
      <MouthTopPictureScreen handlers={handlers} />
    </View>
  )
}

MouthTopPictureController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default MouthTopPictureController
