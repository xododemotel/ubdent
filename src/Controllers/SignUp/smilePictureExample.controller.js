import React, { useEffect, useState } from 'react'
import { View } from 'react-native'
import useGlobal from '../../Store'
import SmilePictureExampleScreen from '../../Screens/SignUp/smilePictureExample.screen'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function SmilePictureExampleController () {
  const navigate = useNavigation()
  const [, actions] = useGlobal()
  const [localState, setLocalState] = useState({ showImage: false })

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
    } catch (error) {
      console.log(error)
    }
  }, [])

  const handleOnEndVideo = () => {
    try {
      setLocalState({
        ...localState,
        showImage: true
      })
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = () => {
    try {
      resetStackNavigation(0, ['TakeSmilePicture'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    showImage: localState.showImage,
    video: '5-foto-sorriso-video-exemplo',
    videoExample: 'demonstracao'
  }

  return (
    <View style={Styles.mainContainer}>
      <SmilePictureExampleScreen handlers={handlers} />
    </View>
  )
}

SmilePictureExampleController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default SmilePictureExampleController
