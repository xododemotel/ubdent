import React from 'react'
import { View, Alert } from 'react-native'
import ShowMouthTopPictureScreen from '../../Screens/SignUp/showMouthTopPicture.screen'
import { useNavigation } from 'react-navigation-hooks'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { resetStackNavigation } from '../../Utils/navigation.util'

function ShowMouthTopPictureController () {
  const navigate = useNavigation()
  const [state, actions] = useGlobal()

  const handleNavigation = (destination) => {
    try {
      resetStackNavigation(0, [destination], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handleUploadFile = async () => {
    try {
      const [, response] = await actions.uploadFile(state.anamnese.mouthTopPicture, 'top')
      if (response) {
        handleNavigation('MouthBottomPicture')
      } else {
        Alert.alert(
          'OPS',
          'Não foi possível enviar sua foto. Por favor, tire outra foto e tente novamente.',
          [
            { text: 'OK' }
          ],
          { cancelable: false }
        )
      }
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    uploadFile: handleUploadFile,
    onPress: handleNavigation
  }

  return (
    <View style={Styles.mainContainer}>
      <ShowMouthTopPictureScreen handlers={handlers} />
    </View>
  )
}

ShowMouthTopPictureController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default ShowMouthTopPictureController
