import React, { useEffect } from 'react'
import { View } from 'react-native'
import useGlobal from '../../Store'
import MouthBottomPictureScreen from '../../Screens/SignUp/mouthBottomPicture.screen'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function MouthBottomPictureController () {
  const navigate = useNavigation()
  const [, actions] = useGlobal()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
    } catch (error) {
      console.log(error)
    }
  }, [])

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', false)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      resetStackNavigation(0, ['TakeMouthBottomPicture'], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    video: '7-foto-dentes-de-baixo'
  }

  return (
    <View style={Styles.mainContainer}>
      <MouthBottomPictureScreen handlers={handlers} />
    </View>
  )
}

MouthBottomPictureController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default MouthBottomPictureController
