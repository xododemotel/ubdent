import React, { useEffect } from 'react'
import { View, Keyboard, Alert } from 'react-native'
import UserDataScreen from '../../Screens/SignUp/userData.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function UserDataController () {
  const [state, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', 'disabled', true)
      actions.updateComponentState('inputPrimary', 'visible', false)
      actions.updateComponentState('inputPrimary', 'autofocus', false)
    } catch (error) {
      console.log(error)
    }
  }, [])

  useEffect(() => {
    try {
      if (state.user.email !== null && state.user.document !== null && state.component.buttonPrimary.disabled === true) {
        actions.updateComponentState('buttonPrimary', 'disabled', false)
      }
      if (state.user.email === '' || state.user.email === null || state.user.document === '' || state.user.document === null) {
        actions.updateComponentState('buttonPrimary', 'disabled', true)
      }
    } catch (error) {
      console.log(error)
    }
  }, [state.user.email, state.user.document])

  useEffect(() => {
    if (state.user.phone.length < 6) {
      actions.updateUserState('phone', '+55')
    }

    if (state.user.phone.length === 19) {
      this.emailRef.focus()
    }
  }, [state.user.phone.length])

  useEffect(() => {
    if (state.user.birthDay.length === 2) {
      this.birthMonthRef.focus()
    }
  }, [state.user.birthDay.length])

  useEffect(() => {
    if (state.user.birthMonth.length === 2) {
      this.birthYearRef.focus()
    }
  }, [state.user.birthMonth.length])

  useEffect(() => {
    if (state.user.birthYear.length === 4) {
      handleMaskBirthDate()
    }
  }, [state.user.birthYear.length])

  const handleOnChangeText = (property, value) => {
    try {
      actions.updateUserState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnEndVideo = () => {
    try {
      actions.updateComponentState('inputPrimary', 'visible', true)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnPress = async () => {
    try {
      Keyboard.dismiss()
      const [, response] = await actions.createNewUser()

      if (response) {
        resetStackNavigation(0, ['UserSymptons'], navigate)
      } else {
        Alert.alert(
          'CPF JÁ EXISTE',
          'O número do CPF informado já existe. Por favor tente novamente com outro número.',
          [
            {
              text: 'OK',
              onPress: () => {
                actions.clearUserState()
                resetStackNavigation(0, ['Fork'], navigate)
              }
            }
          ],
          { cancelable: false }
        )
      }
    } catch (error) {
      console.log(error)
    }
  }

  const handleMaskCPF = (cpf) => {
    try {
      cpf = cpf.replace(/\D/g, '')
      cpf = cpf.replace(/(\d{3})(\d)/, '$1.$2')
      cpf = cpf.replace(/(\d{3})(\d)/, '$1.$2')
      cpf = cpf.replace(/(\d{3})(\d{1,2})$/, '$1-$2')

      actions.updateUserState('document', cpf)
    } catch (error) {
      console.log(error)
    }
  }

  const handleMaskPhone = (phone) => {
    try {
      phone = phone.replace(/\D/g, '')
      phone = phone.replace(/^(\+55)(\d)/, '$1')
      phone = phone.replace(/(\d{2})(\d)/, '+$1 ($2')
      phone = phone.replace(/(\d{2})(\d)/, '$1) $2')
      phone = phone.replace(/(\d{5})(\d{1,4})$/, '$1-$2')

      actions.updateUserState('phone', phone)
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnlyNumbers = (property, value) => {
    try {
      value = value.replace(/\D/g, '')

      actions.updateUserState(property, value)
    } catch (error) {
      console.log(error)
    }
  }

  const handleMaskBirthDate = () => {
    try {
      this.documentRef.focus()
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onChangeText: handleOnChangeText,
    onEndVideo: handleOnEndVideo,
    onPress: handleOnPress,
    maskCPF: handleMaskCPF,
    maskPhone: handleMaskPhone,
    maskBirthDate: () => handleMaskBirthDate,
    onlyNumbers: handleOnlyNumbers,
    video: '2-dados-usuario'
  }

  return (
    <View style={Styles.mainContainer}>
      <UserDataScreen handlers={handlers} />
    </View>
  )
}

UserDataController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default UserDataController
