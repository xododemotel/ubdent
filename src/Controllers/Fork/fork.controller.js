import React, { useEffect } from 'react'
import { View } from 'react-native'
import ForkScreen from '../../Screens/Fork/fork.screen'
import useGlobal from '../../Store'
import Styles from '../../Styles'
import { useNavigation } from 'react-navigation-hooks'
import { resetStackNavigation } from '../../Utils/navigation.util'

function ForkController () {
  const [, actions] = useGlobal()
  const navigate = useNavigation()

  useEffect(() => {
    try {
      actions.updateComponentState('buttonPrimary', { disabled: false })
    } catch (error) {
      console.log(error)
    }
  }, [])

  const handleOnPress = (model) => {
    try {
      resetStackNavigation(0, [model], navigate)
    } catch (error) {
      console.log(error)
    }
  }

  const handlers = {
    onPress: handleOnPress
  }

  return (
    <View style={Styles.mainContainer}>
      <ForkScreen handlers={handlers} />
    </View>
  )
}

ForkController.navigationOptions = {
  header: null,
  gesturesEnabled: false
}

export default ForkController
