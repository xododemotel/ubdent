const generalProps = {
  disabled: false,
  visible: true
}

const inputProps = {
  autoFocus: true,
  enablesReturnKeyAuto: true,
  keyboardType: {
    default: 'default',
    numeric: 'numeric',
    number: 'number-pad',
    email: 'email-address',
    phone: 'phone-pad'
  }
}

const component = {
  buttonPrimary: {
    ...generalProps
  },
  buttonSecondary: {
    ...generalProps
  },
  inputPrimary: {
    ...generalProps,
    ...inputProps
  }
}

export default component
