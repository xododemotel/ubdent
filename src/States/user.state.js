const user = {
  id: null,
  name: null,
  document: '222.222.222-22',
  birthDay: '',
  birthMonth: '',
  birthYear: '',
  email: null,
  genderList: [
    {
      value: 'male',
      label: 'Macho'
    },
    {
      value: 'female',
      label: 'Vadia'
    },
    {
      value: 'undefined',
      label: 'Não sabe o que quer da vida'
    }
  ],
  gender: null,
  phone: '+55'
}

export default user
