import user from './user.state'
import component from './component.state'
import config from './config.state'
import anamnese from './anamnese.state'
import endpoint from './endpoint.state'

const state = {
  user,
  component,
  config,
  anamnese,
  endpoint
}

export default state
