const endpoint = {
  createUser: {
    method: 'post',
    endpoint: 'ubdent/v0/anamnese'
  },
  uploadFile: {
    method: 'patch',
    endpoint: 'ubdent/v0/anamnese/{{CPF}}/files'
  },
  addMedicalData: {
    method: 'patch',
    endpoint: 'ubdent/v0/anamnese/{{CPF}}'
  }
}

export default endpoint
