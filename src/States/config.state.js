const config = {
  mongodbStich: {
    clientObj: null,
    appId: 'ubdent-cduqw',
    userId: null,
    database: 'ubdent',
    appClient: 'mongodb-atlas'
  },
  api: {
    baseURL: 'https://viniciusnovo.dev:19387/',
    key: '1e8bbcf1-894e-4147-9a76-0be5c566cde1'
  }
}

export default config
