const video = {
  rate: 1,
  volume: 1,
  muted: false,
  resizeMode: 'contain',
  duration: 0.0,
  currentTime: 0.0,
  paused: false,
  pickerValueHolder: '1.0',
  pausedText: 'Play',
  hideControls: true
}

export default video
