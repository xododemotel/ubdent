const anamnese = {
  userSymptons: null,
  smilePicture: null,
  mouthTopPicture: null,
  mouthBottomPicture: null,
  lastVisitDay: '',
  lastVisitMonth: '',
  lastVisitYear: '',
  dentistFear: false,
  doctorLastVisitDay: '',
  doctorLastVisitMonth: '',
  doctorLastVisitYear: '',
  healthProblems: false,
  healthProblemsDetails: null,
  takeMedication: false,
  takeMedicationDetails: null,
  haveAllergy: false,
  haveAllergyDetails: null,
  lowPressure: false,
  normalPressure: false,
  highPressure: false,
  diabetes: false,
  hearthProblem: false,
  pregnant: false
}

export default anamnese
